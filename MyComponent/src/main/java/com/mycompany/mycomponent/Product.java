/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author Waratchapon Ponpiya
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "เอสเพรสโซ่ร้อน 1 ", 45, "image1.png"));
        list.add(new Product(1, "เอสเพรสโซ่เย็น 1 ", 55, "image2.png"));
        list.add(new Product(1, "เอสเพรสโซ่ปั่น ", 65, "image3.png"));
        list.add(new Product(1, "เอเมริกาโน่ร้อน 1 ", 45, "image1.png"));
        list.add(new Product(1, "เอเมริกาโน่ร้อน 2 ", 50, "image2.png"));
        list.add(new Product(1, "เอเมริกาโน่ร้อน 3 ", 55, "image3.png"));
        list.add(new Product(1, "เอเมริกาโน่ร้อน 4 ", 60, "image1.png"));
        list.add(new Product(1, "เอเมริกาโน่ร้อน 5 ", 65, "image2.png"));
        list.add(new Product(1, "เอเมริกาโน่ร้อน 6 ", 70, "image3.png"));
        list.add(new Product(1, "เอเมริกาโน่ร้อน 7 ", 75, "image1.png"));
        return list;

    }

}
